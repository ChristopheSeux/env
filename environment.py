
from os import environ
from os.path import expandvars, exists, splitext, pathsep
import platform
import json


class Env:
    """Basic Object for reading and setting an environment

    Note:
        The env is setted at the __init__.
        Env[k] = v update the env.

    Args:
        *envs: List of .json or .yml env files.

    Attributes:
        platform (str): The owner platform Mac, Linux or Windows.
        sources (list): List of env files as string.
        data (dict): The expanded env as a dict
    """

    def __init__(self, *envs):
        self.platform = platform.system()
        if self.platform == 'Darwin':
            self.platform = 'Mac'

        self.sources = [str(e) for e in envs]

        self.data = self._merge_dict([self._read_env(s) for s in envs])

        #self.set()

    def _merge_dict(self, dicts):
        data = {}
        for d in dicts :
            data.update(d)

        return data

    def _read_env_file(self, path):
        with open(path, 'r') as f:
            if splitext(path)[1] == '.json':
                return json.load(f)
            elif path.suffix in ('.yaml', '.yml'):
                import yaml
                return yaml.safe_load(f) 

    def _read_env(self, path):
        if isinstance(path, dict):
            return path

        if not exists(path):
            raise Exception(f'The env "{path}" does not exist')

        data = self._read_env_file(path)

        for p in ['Mac', 'Linux', 'Windows']:
            if not p in data : continue

            if p == self.platform:
                data.update(data.pop(p))
            else :
                del data[p]

        data['ENVS'] = pathsep.join(self.sources)

        return data

    '''
    def expanded(self):
        expanded_env = {}
        for k, v in self.data.items():
            if isinstance(v, (list, tuple)):
                values = environ.get(k)
                values = values.split(pathsep) if values else []
                values += [expandvars(str(i)) for i in v]
                expanded_env[k] = pathsep.join(values)
            else : 
                expanded_env[k] = expandvars(str(v))

        return expanded_env
        '''

    def to_dict(self):
        return self.data.copy()

    def set(self):
        """Set os.environ with the expanded env.

        Note:
            PYTHONPATH is added at the end of the key exist.
        """
        for k, v in self.data.items():
            if isinstance(v, (list, tuple)):
                values = environ.get(k)
                values = values.split(pathsep) if values else []
                values += [expandvars(str(i)) for i in v]
                values = list(set(values))
                environ[k] = pathsep.join(values)
            else :
                environ[k] = expandvars(str(v))

    def __setitem__(self, key, value):
        self.data[key] = value
        #self.set()

    def __getitem__(self, key):
        return self.data[key]

    def items(self):
        return self.data.items()

    def keys(self):
        return self.data.keys()

    def __repr__(self):
        text = '\nEnv(\n'
        for k, v in self.data.items():
            text+= f"    {k} : {v}\n"
        text+= ')\n'

        return text
